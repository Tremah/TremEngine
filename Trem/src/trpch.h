/**
 *  \brief Precompiled header file
 *
 */
#pragma once

//memory
#include <memory>

//timing
#include <chrono>
#include <ctime>

//container
#include <vector>
#include <unordered_map>
#include <array>

//strings and streams
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

//misc
#include <algorithm>
#include <cstdint>
#include <cmath>

//windows specific
#include <Windows.h>

//external libraries

//compiler macros
#include <trem/core/base.hpp>

//engine headers

