﻿#include <trpch.h>
#include "data_definition.h"


namespace Trem::Data
{
  //Render data

  Quad::Quad() : transform_{1.f}, texture_{}
  { 
    vertices_ = defaultVertices(glm::vec4{1.f});
  }

  Quad::Quad(const glm::vec4& color, const glm::mat4& transform, const ShaPtr<Texture> texture) : transform_{transform}, texture_{texture}
  {
    vertices_ = defaultVertices(color);
  }

  Quad::Quad(const glm::vec4& color, ShaPtr<Texture> texture) : texture_{texture}
  {
    vertices_ = defaultVertices(color);

    //define transform  by dimensions of the texture
    glm::mat4 scale = glm::scale(glm::vec3{texture_->dimensions(),1.f}); 
    glm::mat4 translation{1.f};
    glm::mat4 rotation{1.f};
    transform_ = translation * rotation * scale;   
  }

  void Quad::setPosition(const glm::vec3& position)
  {
    TransformContainer transform = decomposeTransform(transform_);
    transform.translation = position;

    transform_ = composeTransform(transform);
  }

  void Quad::setRotation(const float angle, const glm::vec3& axis)
  {
    TransformContainer transform = decomposeTransform(transform_);
    transform.rotation = glm::rotate(glm::radians(angle), axis );

    transform_ = composeTransform(transform);
  }

  void Quad::setScale(const glm::vec3& scale)
  {
    TransformContainer transform = decomposeTransform(transform_);
    transform.scale = scale;

    transform_ = composeTransform(transform);
  }

  std::array<QuadVertex, 4> defaultVertices(const glm::vec4& color)
  {
    std::array<QuadVertex, 4> vertices =
    {
      QuadVertex{glm::vec4{0.f, 0.f, 0.f, 1.0f}, color, glm::vec2{0.0f, 0.0f}}, /** bottom left*/
      QuadVertex{glm::vec4{1.f, 0.f, 0.f, 1.0f}, color, glm::vec2{1.0f, 0.0f}}, /** bottom right*/
      QuadVertex{glm::vec4{1.f, 1.f, 0.f, 1.0f}, color, glm::vec2{1.f, 1.f}},   /** top right*/
      QuadVertex{glm::vec4{0.f, 1.f, 0.f, 1.0f}, color, glm::vec2{0.f, 1.f}}    /** top left*/
    };

    return vertices;
  }

  glm::mat4 composeTransform(const TransformContainer& transformContainer)
  {
    glm::mat4 rotation    = transformContainer.rotation;
    glm::mat4 translation = glm::translate(transformContainer.translation);
    glm::mat4 scale       = glm::scale(transformContainer.scale);
    glm::mat4 transform   = translation * rotation * scale;

    return transform;
  }

  TransformContainer decomposeTransform(const glm::mat4& transform)
  {
    TransformContainer transformContainer{};
    glm::quat orientation;
    glm::decompose(transform, transformContainer.scale, orientation, transformContainer.translation, transformContainer.skew, transformContainer.perspective);
    //convert quaternion to 4x4 matrix
    transformContainer.rotation = glm::toMat4(orientation);

    return transformContainer;
  }

  //Vertex buffer layout
  BufferElement::BufferElement(const std::string& name, const BufferElementType type)
  {
    name_ = name;
    type_ = type;
    size_ = bufferElementTypeSize(type_);

    //calculate the elements offset
    switch (type_)
    {
    case BufferElementType::Bool:
    case BufferElementType::Float:
    case BufferElementType::Int:
      offset_ = 1;
      break;
    case BufferElementType::Float2:
    case BufferElementType::Int2:
      offset_ = 2;
      break;
    case BufferElementType::Float3:
    case BufferElementType::Int3:
      offset_ = 3;
      break;
    case BufferElementType::Float4:
    case BufferElementType::Int4:
      offset_ = 4;
      break;
    case BufferElementType::Mat3:
      offset_ = 9; //3 * 3
      break;
    case BufferElementType::Mat4:
      offset_ = 4 * 4; //4 * 4
      break;
    default:
      TR_ASSERT(type != BufferElementType::None, "Invalid buffer element type provided!")
      break;
    }
  }

  constexpr uint32_t bufferElementTypeSize(BufferElementType type)
  {
    switch (type)
    {
    case BufferElementType::Bool:   return 1;
    case BufferElementType::Float:  return 4;
    case BufferElementType::Float2: return 4 * 2;
    case BufferElementType::Float3: return 4 * 3;
    case BufferElementType::Float4: return 4 * 4;
    case BufferElementType::Int:    return 4;
    case BufferElementType::Int2:   return 4 * 2;
    case BufferElementType::Int3:   return 4 * 3;
    case BufferElementType::Int4:   return 4 * 4;
    case BufferElementType::Mat3:   return 4 * 3 * 3;
    case BufferElementType::Mat4:   return 4 * 4 * 4;
    default:
      TR_ASSERT(type != BufferElementType::None, "No buffer element type provided")
      break;
    }

    return 0;
  }
}

