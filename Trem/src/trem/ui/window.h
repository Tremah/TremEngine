#pragma once

//c++ includes

//external library includes
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

//custom (own) library includes

//game engine includes
#include <trem/ui/input_codes.h>

/**
 *  \brief Parent class for any object within the game
 *
 */
namespace Trem
{
  //opengl, glfw callbacks
  static void glDebugCallback(GLenum source, GLenum type, unsigned int id, GLenum severity, GLsizei length, const char* message, const void* userParam);
  static void glfwErrorCallback(int error, const char* description);
  static void glfwFramebufferSizeCallback(GLFWwindow* pWindow, int pWidth, int pHeight);

  struct WindowData
  {
    uint16_t width_;
    uint16_t height_;
    std::string title_; /**window title*/
  }; /**struct to hold information about the window*/

  class Window
  {
    public:
      Window(const Window&) = default;
      Window(Window&&) = default;
      Window& operator=(const Window&) = default;
      Window& operator=(Window&&) = default;

      Window();
      ~Window();
      //param. constructors
      Window(const uint16_t width, const uint16_t height, const std::string& title);

      //public member variables

      //public member functions
      void init();
      GLFWwindow* nativeWindow() const;
      uint16_t width()  const;
      uint16_t height() const;
      void swapBuffer() const;

      //window input functions
      bool keyPressed(const KeyCode key) const;
      bool mouseButtonPressed(const MouseCode button) const;
      glm::vec2 mousePosition() const;
      float mouseX() const;
      float mouseY() const;      

    protected:
      //protected member variables

      //protected member functions

    private:
      //private member variables
      GLFWwindow* window_;
      static uint8_t windowCount_;

      WindowData windowData_;

      //private member functions
  };
}
