#include <trpch.h>
#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp> 
#include <glm/ext/matrix_clip_space.hpp> 
#include <glm/ext/scalar_constants.hpp> 
#include <glm/gtx/transform.hpp>

#include <trem/renderer/renderer.h>
#include <trem/renderer/shader.h>
#include <trem/renderer/texture_manager.h>
#include <trem/ui/window.h>
#include <trem/util/time.h>

int main(const int argc, const char* args[])
{
  //set up window
  constexpr uint16_t screenWidth = 1200;
  constexpr uint16_t screenHeight = 900;
  Trem::Window window{screenWidth, screenHeight, "SandBox"};
  window.init();

  //set up renderer
  Trem::Renderer renderer{};
  renderer.init();
  
  glm::mat4 model = glm::mat4{1.f};  
  glm::mat4 view{1.f};
  glm::mat4 projection{glm::ortho<float>(0.f, static_cast<float>(window.width()), 0.f, static_cast<float>(window.height()), 0.f, 1.f)};
  glm::mat4 mvp = projection * view * model;
  renderer.activeShader()->uploadUniformMat4("uMvp", mvp);  
  
  //timing components and game loop
  auto currentTime  = Trem::Util::now();
  auto previousTime = Trem::Util::now();
  static constexpr float desiredFps = 60.f;
  static constexpr float desiredDTime = 1.f / desiredFps;
  float dTime = 0;
  float dAccumulator = 0;
  bool run = true;
  
  //game objects
  glm::vec4 quadColor{1.0f, 1.0f, 1.0f, 1.0f};
  Trem::Data::Quad cowboy{quadColor, renderer.textureManager().texture("cowboy_large")};
  glm::vec4 quadColor2{1.f, 1.f, 1.0f, 1.f};
  Trem::Data::Quad white{quadColor2, renderer.textureManager().texture("white_color")};
  white.setScale(glm::vec3{100.0f, 100.f, 1.f});

  //test/debug
  while(run && !glfwWindowShouldClose(window.nativeWindow()))
  {
    previousTime = currentTime;
    currentTime = Trem::Util::now();
    dTime = Trem::Util::deltaTime(previousTime, currentTime);
    dAccumulator += dTime;   

    if(dAccumulator >= desiredDTime)
    {
      glfwPollEvents();      

      renderer.beginScene();
      
      white.setPosition({200.f, 350.f, 0.f});
      renderer.drawQuad(white);      
      cowboy.setPosition({200.f, 350.f, 0.f});
      renderer.drawQuad(cowboy);

      renderer.endScene();
      
      window.swapBuffer();
      
      dAccumulator = 0;
    }     

    if(window.keyPressed(Trem::Input::Escape))
    {
      run = false;
    }
  }
}
