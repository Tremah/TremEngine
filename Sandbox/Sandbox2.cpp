﻿#include <trpch.h>


#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include <trem/core/base.hpp>
#include <trem/renderer/renderer.h>
#include <trem/renderer/shader.h> 
#include <trem/renderer/shader_library.h>
#include <trem/ui/window.h>
#include <trem/util/time.h>
#include <trem/renderer/data_definition.h>

void printContainer(const Trem::Data::TransformContainer& tContainer)
{
  std::cout << "Container:" << std::endl << "==========" << std::endl << std::endl; 
  std::cout << "translation = x: " << tContainer.translation.x << ", y: " << tContainer.translation.y <<  ", z: " << tContainer.translation.z  << std::endl;
  std::cout << "scale = x: "       << tContainer.scale.x << ", y: " << tContainer.scale.y <<  ", z: " << tContainer.scale.z  << std::endl;
  std::cout << "rotation i = x: "  << tContainer.rotation[0].x << ", y: " << tContainer.rotation[0].y <<  ", z: " << tContainer.rotation[0].z  << std::endl;
  std::cout << "rotation j = x: "  << tContainer.rotation[1].x << ", y: " << tContainer.rotation[1].y <<  ", z: " << tContainer.rotation[1].z  << std::endl << std::endl;
}

void printTransform(glm::mat4& transform)
{    
  std::cout << "Transform Matrix:" << std::endl << "==========" << std::endl << std::endl;
  for(int i = 0; i < 4; i++)
  {
    std::cout << "Column " << i << " = x: " << transform[i].x << ", y: " << transform[i].y << ", z: " << transform[i].z << std::endl;
  }
}

int main(const int argc, const char* args[])
{ 
  /*
  Trem::Data::TransformContainer tContainerOrig{};
  tContainerOrig.scale       = glm::vec3{200.f, 200.f, 1.f};
  tContainerOrig.translation = glm::vec3{500.f, 500.f, 1.f};
  tContainerOrig.rotation    = glm::rotate(glm::radians(45.f), glm::vec3{0.f, 0.f, 1.f});
  tContainerOrig.perspective = glm::vec4{1.f};
  tContainerOrig.skew        = glm::vec3{1.f};
  
  std::cout << "container before decomposition" << std::endl;
  printContainer(tContainerOrig);

  glm::mat4 transform = Trem::Data::composeTransform(tContainerOrig);
  std::cout << "transform: " << std::endl;
  printTransform(transform);

  Trem::Data::TransformContainer tContainer = Trem::Data::decomposeTransform(transform);
  
  std::cout << "container after decomposition" << std::endl;
  printContainer(tContainer);   
   */


  Trem::Data::Quad quad{};
  quad.setScale(glm::vec3{100.f, 100.f, 1.f});
  quad.setPosition(glm::vec3{300.f, 300.f, 1.f});
    
 

  
  std::cin.get();
}