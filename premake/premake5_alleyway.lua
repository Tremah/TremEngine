project "AlleyWay"
  kind "ConsoleApp"
  language "C++"
  architecture "x86_64"
  location ("../%{prj.name}/")
  
  targetdir ("..bin/bin/" .. outputdir .. "/%{prj.name}")
  objdir    ("..bin/bin-int/" .. outputdir .. "/%{prj.name}")
  debugdir  ("../%{prj.name}/")
  
  includedirs
  {
    "../%{prj.name}/src/",
    "../Trem/src",
    "../vendor/util",
    "../vendor/mathlib",
    "../vendor/glad/include/",
    "../vendor/glfw/include/",
    "../vendor/glm"
  }
  
  files
  {
    "../%{prj.name}/*.cpp",
    "../%{prj.name}/*.hpp",
    "../%{prj.name}/*.h",
    "../%{prj.name}/src/**.h",
    "../%{prj.name}/src/**.hpp",
    "../%{prj.name}/src/**.cpp",  
  }  
  
  links { "Trem", "Mathlib", "Util", "glad", "glfw", "glm" }
  
  filter "system:windows"
    staticruntime "on"
		systemversion "latest"
    cppdialect "C++17"

	filter "configurations:Debug"
		defines "AW_DEBUG=1"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "AW_RELEASE=1"
		runtime "Release"
		optimize "on"