project "Util"
	kind "StaticLib"
	language "C++"
	architecture "x86_64"
  location ("../vendor/%{prj.name}/")
  
  targetdir ("..bin/bin/" .. outputdir .. "/%{prj.name}")
  objdir    ("..bin/bin-int/" .. outputdir .. "/%{prj.name}")
  debugdir ("../vendor/%{prj.name}/%{prj.name}/")
  
  files
  {
    "../vendor/%{prj.name}/%{prj.name}/**.h", 
    "../vendor/%{prj.name}/%{prj.name}/**.hpp", 
    "../vendor/%{prj.name}/%{prj.name}/**.cpp", 
    "../vendor/%{prj.name}/%{prj.name}/**.inl", 
  }
  
  includedirs { "../vendor/%{prj.name}/" }

  filter "system:windows"
    cppdialect "C++17"
    staticruntime "on"
    systemversion "latest"
  
  filter "configurations:Debug"
    defines "UT_DEBUG"
    symbols "On"
  
  filter "configurations:Release"
    defines "UT_RELEASE"
    symbols "On"